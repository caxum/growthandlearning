﻿using System;
using System.Collections.Generic;
using System.Text;

using Amazon.DynamoDBv2.DataModel;

namespace DynamicDBQueryConsole.Model
{
    [DynamoDBTable("Customer")]
    public class Customer
    {
        //[DynamoDBHashKey("CustomerId")]
        //[DynamoDBGlobalSecondaryIndexRangeKey("CustomerId")]
        //[DynamoDBRangeKey]
        [DynamoDBHashKey("CustomerId")]
        public string CustomerId { get; set; }
        // [DynamoDBGlobalSecondaryIndexHashKey]
        //

        [DynamoDBProperty("FirstName")]
        public string FirstName { get; set; }

        [DynamoDBProperty("LastName")]
        public string LastName { get; set; }

    }
}
